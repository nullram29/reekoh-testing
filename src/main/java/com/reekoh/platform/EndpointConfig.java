package com.reekoh.platform;

import java.util.Collections;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;

import com.consol.citrus.actions.AbstractTestAction;
import com.consol.citrus.container.SequenceBeforeSuite;
import com.consol.citrus.context.TestContext;
import com.consol.citrus.dsl.design.TestDesigner;
import com.consol.citrus.dsl.design.TestDesignerBeforeSuiteSupport;
import com.consol.citrus.dsl.endpoint.CitrusEndpoints;
import com.consol.citrus.http.client.HttpClient;
import com.consol.citrus.message.MessageType;
import com.consol.citrus.variable.GlobalVariables;
import com.consol.citrus.xml.namespace.NamespaceContextBuilder;

@Configuration
public class EndpointConfig {
	private GlobalVariables gvl = new GlobalVariables();
	@Bean
	public HttpClient devClient() {
		return CitrusEndpoints.http()
				.client().requestUrl("https://dev-api.reekoh.io").build();

	}
	
	@Bean
	public GlobalVariables globalVar() {
		return gvl;
	}

	@Bean
	public NamespaceContextBuilder namespaceContextBuilder() {
		NamespaceContextBuilder namespaceContextBuilder = new NamespaceContextBuilder();
		namespaceContextBuilder.setNamespaceMappings(Collections.singletonMap("xh", "http://www.w3.org/1999/xhtml"));
		return namespaceContextBuilder;
	}

	@Bean
   	public SequenceBeforeSuite beforeSuite() {
   		return new TestDesignerBeforeSuiteSupport() {
   			@Override
   			public void beforeSuite(TestDesigner designer) {
   				designer.http().client(devClient()).send().post("/auth/user").messageType(MessageType.JSON)
                   .contentType("application/json").payload("{ \"email\": \"test@reekoh.com\", " + " \"password\": \"123456\" }");
                   designer.http().client(devClient())
                   .receive()
                   .response(HttpStatus.OK)
                   .messageType(MessageType.JSON)
                   .extractFromPayload("$.idToken", "payload_token");
                   
                   designer.http().client(devClient()).send()
                   .get("/users?page=1&limit=25&sort=-createdDate&expand=user%20role&q=test@reekoh.com")
                   .accept("application/json")
                   .contentType("application/json")
                   .header("Authorization", "Bearer ${payload_token}");
                   designer.http().client(devClient()).receive()
                   .response(HttpStatus.OK).messageType(MessageType.JSON)
                   .extractFromPayload("$.data[0]._id", "userId");
       	        designer.action(new AbstractTestAction() {
       	            @Override public void doExecute(TestContext testContext) {
       	            	gvl.getVariables().put("global_auth_token", testContext.getVariableObject("payload_token").toString());
       	            	gvl.getVariables().put("global_user_id", testContext.getVariableObject("userId").toString());
       	            }
       	        });
   			}

   		};
   	}
}
