/*
 * Copyright 2006-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.reekoh.platform;

import com.consol.citrus.annotations.CitrusTest;
import com.consol.citrus.dsl.testng.TestNGCitrusTestDesigner;
import com.consol.citrus.http.client.HttpClient;
import com.consol.citrus.message.MessageType;
import com.consol.citrus.variable.GlobalVariables;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.testng.annotations.Test;

/**
 * @author nullram advincula
 */
@Test
public class SimulatorRestIT extends TestNGCitrusTestDesigner{

   

    /** Test Http REST client */
    @Autowired
    private HttpClient devClient;
    
    /**
     * Sends a request tor recieve SystemLogs.
     * 
     */
    @CitrusTest
    public void GetSystemLog() {
        http().client(devClient)
                .send()
                .get("/system-logs/?recordId=${global_user_id}")
                .accept("application/json")
                .contentType("application/json")
                .header("Authorization", "Bearer ${global_auth_token}");

        http().client(devClient)
                .receive()
                .response(HttpStatus.OK)
                .messageType(MessageType.JSON)
                .validate("$.totalPages", "1")
                .validate("$.totalRecords", "5")
                .validate("$.docsPerPage", "25")
                .validate("$.currentPage", "1");
    }

}
