# Set up Citrus and use Maven 

## required on your local machine
 * Java 8 or higher
 * Java IDE eclipse
 * Maven 3.0.x or higher
**Intalling Java 8**
```
  sudo apt update
  sudo apt install openjdk-8-jdk
```  
To verify if successfully deployed.

```
java -version
```
set up JAVA_HOME path.
```
export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
```
to verify if successfully set 
```
echo $JAVA_HOME
```
--------------------------
**Installing Eclipse IDE**

installing JRE java runtime environment

 ```
 sudo apt install install default-jre
 ```

 ```
 sudo snap install --classic eclipse
 ```

-------------------------


**Installing Maven**

download link http://maven.apache.org/download.cgi


extract
```
tar xzvf apache-maven-3.6.2-bin.tar.gz

```

move 

````
sudo cp -r "source" "destination"
sudo cp -r "path of extracted file"/apache-maven-3.6.2 /opt

````
create a symbolic link maven 
```
sudo ln -s /opt/apache-maven-3.6.0 /opt/maven


```
open and edit .bashrc or .zshrc with your text editor modify path and save
````
PATH=$PATH:/snap/bin:/opt/maven/bin
````

to verify
```
mvn -version
```


# Creating Citrus Test  via Maven

go to the eclipse workspace
```
cd eclipse-workspace
```

project scaffold
```
mvn archetype:generate -Dfilter=com.consol.citrus.archetypes:citrus
```

Sample OUTPUT 
```

[INFO] Scanning for projects...
[INFO] 
[INFO] ------------------< org.apache.maven:standalone-pom >-------------------
[INFO] Building Maven Stub Project (No POM) 1
[INFO] --------------------------------[ pom ]---------------------------------
[INFO] 
[INFO] >>> maven-archetype-plugin:3.1.2:generate (default-cli) > generate-sources @ standalone-pom >>>
[INFO] 
[INFO] <<< maven-archetype-plugin:3.1.2:generate (default-cli) < generate-sources @ standalone-pom <<<
[INFO] 
[INFO] 
[INFO] --- maven-archetype-plugin:3.1.2:generate (default-cli) @ standalone-pom ---
[INFO] Generating project in Interactive mode
[INFO] No archetype defined. Using maven-archetype-quickstart (org.apache.maven.archetypes:maven-archetype-quickstart:1.0)
Choose archetype:
1: remote -> com.consol.citrus.archetypes:citrus-quickstart (Citrus quickstart project)
2: remote -> com.consol.citrus.archetypes:citrus-quickstart-jms (Citrus quickstart project with JMS consumer and producer)
3: remote -> com.consol.citrus.archetypes:citrus-quickstart-soap (Citrus quickstart project with SOAP client and server)
4: remote -> com.consol.citrus.archetypes:citrus-simulator-archetype-jms (Archetype for Citrus JMS simulator)
5: remote -> com.consol.citrus.archetypes:citrus-simulator-archetype-mail (Archetype for Citrus mail simulator)
6: remote -> com.consol.citrus.archetypes:citrus-simulator-archetype-rest (Archetype for Citrus REST simulator)
7: remote -> com.consol.citrus.archetypes:citrus-simulator-archetype-swagger (Archetype for Citrus Swagger auto generated REST simulator)
8: remote -> com.consol.citrus.archetypes:citrus-simulator-archetype-ws (Archetype for Citrus SOAP simulator)
9: remote -> com.consol.citrus.archetypes:citrus-simulator-archetype-wsdl (Archetype for Citrus WSDL auto generated SOAP simulator)
Choose a number or apply filter (format: [groupId:]artifactId, case sensitive contains): : 6


```
select 6(Citrus REST simulator)

define the values on the following see output below

```
Define value for property 'groupId': com.reekoh.platform.citrus         
Define value for property 'artifactId': citrus-testing
Define value for property 'version' 1.0-SNAPSHOT: : 
Define value for property 'package' com.reekoh.platform.citrus: : 
Confirm properties configuration:
groupId: com.reekoh.platform.citrus
artifactId: reekoh-platform-testing
version: 1.0-SNAPSHOT
package: com.reekoh.platform.citrus
 Y: : Y

```

open the project via eclipse

follow steps
 * open eclipse IDE
 * on the project explorer right-click import>Maven>Existing Maven Project click "next"
 * Browse the created project eg:`reekoh-platform-testing` click finish.

 create the java file reekoh-platform-testing/scr/test/java/com/reekoh/platform/citrus/EndpointConfig.java see sample code below

 ```

 ```


Citrus REST Simulator ![Logo][1]
================

This is a standalone simulator application for REST messaging.

Clients are able to access the simulator endpoints and the simulator responds with predefined response
messages according to its scenarios. The simulator response logic is very powerful and enables us to simulate 
any kind of server interface.

Read the simulator [user manual](https://citrusframework.org/citrus-simulator/) for more information.

Message processing
---------

First of all the simulator identifies the simulator scenario based on a mapping key that is extracted from the incoming request. Based
on that operation key the respective simulator scenario is executed.

There are multiple ways to identify the simulator scenario from incoming request messages:

* Message-Type: Each request message type (XML root QName) results in a separate simulator scenario
* REST request mappings: Identifies the scenario based on Http method and resource path on server
* SOAP Action: Each SOAP action value defines a simulator scenario
* Message Header: Any SOAP or Http message header value specifies a new simulator scenario
* XPath payload: An XPath expression is evaluated on the message payload to identify the scenario

Once the simulator scenario is identified the respective test logic builder is executed. The Citrus test logic provides
proper response messages as a result to the calling client. The response messages can hold dynamic values and the
simulator is able to perform complex response generating logic. The test logic is built in Java classes that use the Citrus test
DSL for defining the simulator scenario steps.

Quick start
---------

You can build the simulator application locally with Maven:

```
mvn clean install
```

This will compile and package all resources for you. Also some prepared Citrus integration tests are executed during the build. 
After the successful build you are able to run the simulator with:

```
mvn spring-boot:run
```

Open your browser and point to [http://localhost:8080](http://localhost:8080). You will see the simulator user interface with all available scenarios and 
latest activities. 

You can execute the Citrus integration tests now in order to get some interaction with the simulator. Open the Maven project in your favorite IDE and
run the tests with TestNG plugins. You should see the tests calling operations on the simulator in order to receive proper responses. The simulator user interface should track those
interactions accordingly.

Information
---------

Read the [user manual](https://citrusframework.org/citrus-simulator/) for detailed instructions and features.
For more information on Citrus see [citrusframework.org][2], including a complete [reference manual][3].

 [1]: https://citrusframework.org/img/brand-logo.png "Citrus"
 [2]: https://citrusframework.org
 [3]: https://citrusframework.org/reference/html/

